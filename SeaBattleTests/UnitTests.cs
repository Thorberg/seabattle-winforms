﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SeaBattle.Controller;
using SeaBattle.Model;
using System.Linq;

namespace SeaBattleTests
{
    [TestClass]
    public class UnitTests
    {
        [TestMethod]
        public void MapModel_AllShipGenerated()
        {
            //Arrange
            
            //Act
            MapModel map = new MapModel(10);
            map.GenerateShips();

            //Assert
            Assert.AreEqual(12, map.TotalShips);
        }

        [TestMethod]
        public void MapModel_ShipVicinity()
        {
            //Arrange

            //Act
            MapModel map = new MapModel(10);
            map.GenerateShips();

            //Assert
            foreach (Ship ship in map.ships)
                foreach(TileModel tile in ship.GetVicinity(map))
                    Assert.AreEqual(false, tile.ContainShip);
        }

        [TestMethod]
        public void MapModel_LargeTest()
        {
            int failedGeneration = 0;
            
            for (int i = 0; i < 20000; i++)
            {
                try
                {
                    MapModel_AllShipGenerated();
                    MapModel_ShipVicinity();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    failedGeneration++;
                }
            }

            Assert.AreEqual(0, failedGeneration);
        }

        [TestMethod]
        public void AI_SelectTile()
        {
            //Arrange
            var board = new MapModel(10);
            board.GenerateShips();

            Player player1 = new Player
            {
                Nickname = "TestPlayer",
                Board = board
            };

            Player player2 = new Player
            {
                Nickname = "TestPlayer",
                Opponent = player1
            };

            ArtificialIntelligence AI = new ArtificialIntelligence(player2);

            var ship = board.ships.First(x => x.size > 1);
            var shipTile = ship.GetTiles(board).First();
            shipTile.Reveal();
            ship.ReceiveHit();

            AI.lastMove = shipTile;

            //Act
            int shipTilesSelected = 0;
            for (int i = 0; i < 4; i++) {
                var tile = AI.SelectTile();
                if (tile.ContainShip && tile.ship.Equals(ship))
                    shipTilesSelected++;
            }

            //Assert
            Assert.AreNotEqual(0, shipTilesSelected);
        }
    }
}
