﻿using SeaBattle.Controller;
using SeaBattle.Utililty;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace SeaBattle.Model
{
    public class MapModel
    {
        public Player owner;
        public int size;

        public int MaxGenAttempts = 3;

        internal TileModel[,] tiles;
        public List<Ship> ships = new List<Ship>();

        public int TotalShips => ships.Count;
        public int DestroyedShips => ships.Where(s => s.IsDestroyed()).ToList().Count;

        public MapModel(int size)
        {
            this.size = size;
            tiles = new TileModel[size, size];

            for (int i = 0; i < size; i++)
                for (int j = 0; j < size; j++)
                {
                    TileModel tile = new TileModel
                    {
                        fogOfWar = true,
                        x = i,
                        y = j
                    };

                    tiles[i, j] = tile;
                }
        }

        public void AddShip(Ship ship)
        {
            ships.Add(ship);
            foreach (var tile in ship.GetTiles(this))
                tile.ship = ship;
        }

        public bool IsValidTile(int x, int y)
        {
            return x >= 0 && x < size && y >= 0 && y < size;
        }

        public void GenerateShips()
        {
            int attempt = 0;
            while (attempt < MaxGenAttempts) {
                attempt++;
                SetToDefault();

                try
                {
                    for (int i = 0; i < 1; i++)
                        GenerateShip(4, RandomRotation());
                    for (int i = 0; i < 2; i++)
                        GenerateShip(3, RandomRotation());
                    for (int i = 0; i < 4; i++)
                        GenerateShip(2, RandomRotation());
                    for (int i = 0; i < 5; i++)
                        GenerateShip(1, RandomRotation());
                    return;
                }
                catch (Exception ex)
                {
                    Program.Logger.LogError(ex.Message);
                }
            }

            throw new Exception("Board generation failed!");
        }

        private void SetToDefault()
        {
            foreach (var tile in tiles)
            {
                tile.ship = null;
                tile.fogOfWar = true;
            }
            ships.Clear();
        }

        private void GenerateShip(int shipSize, Rotation rotation)
        {
            var locations = ValidShipLocations(shipSize, rotation);
            Program.Logger.LogInfo("Ship Locations: " + locations.Count);
            if (locations.Count == 0)
                throw new Exception("No possible ship locations..");

            var location = locations[Program.Random.Next(0, locations.Count)];

            Program.Logger.LogInfo("New Ship.. size="+shipSize+" ; Location= ["+location.X + " " + location.Y+"]");
            var ship = new Ship(location, shipSize, rotation);
            AddShip(ship);
        }

        public Rotation RandomRotation()
        {
            var r = Program.Random.Next(0, 2);
            if (r == 0)
                return Rotation.Horizontal;
            else
                return Rotation.Vertical;
        }

        public List<Point> ValidShipLocations(int shipSize, Rotation rotation)
        {
            List<Point> list = new List<Point>();

            for (int i = 0; i < size; i++)
                for (int j = 0; j < size; j++)
                {
                    var location = new Point(i, j);
                    if (IsValidShipLocation(shipSize, rotation, location))
                        list.Add(location);
                }
            return list;
        }

        public bool IsValidShipLocation(int shipSize, Rotation rotation, Point location)
        {
            int x = location.X;
            int y = location.Y;
            for (int i = 0; i < shipSize; i++)
            {
                if (!IsValidTile(x, y))
                    return false;
                if (tiles[x, y].ContainShip || tiles[x, y].IsShipNear(this))
                    return false;

                if (rotation == Rotation.Horizontal)
                    x++;
                else
                    y++;
            }
            return true;
        }

        public void RevealShipVicinity(Ship ship)
        {
            foreach (var tile in ship.GetVicinity(this))
            {
                tile.Reveal();
            }
        }

        public List<TileModel> GetTiles() {
            var allTiles = new List<TileModel>();
            foreach (var tile in tiles)
                allTiles.Add(tile);
            return allTiles;
        }

        public int GetScore()
        {
            int score = 0;
            foreach (TileModel tile in tiles)
            {
                if (tile.IsRevealed && tile.ContainShip)
                    score += 25;
                if (tile.IsRevealed && !tile.ContainShip)
                    score -= 5;
            }

            score += DestroyedShips * 100;
            return score;
        }
    }
}