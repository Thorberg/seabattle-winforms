﻿using System.Collections.Generic;

namespace SeaBattle.Model
{
    public class TileModel
    {
        internal bool fogOfWar;

        public int x, y;
        public Ship ship;

        public bool ContainShip => ship != null;
        public bool IsRevealed => fogOfWar == false;

        public void Reveal()
        {
            fogOfWar = false;
        }

        public bool IsShipNear(MapModel map)
        {
            foreach (var tile in GetVicinity(map))
            {
                if (tile.ContainShip)
                    return true;
            }
            return false;
        }

        public List<TileModel> GetClosestVicinity(MapModel map)
        {
            List<TileModel> neighbors = new List<TileModel>();

            if (map.IsValidTile(x - 1, y))
                neighbors.Add(map.tiles[x - 1, y]);
            if (map.IsValidTile(x + 1, y))
                neighbors.Add(map.tiles[x + 1, y]);
            if (map.IsValidTile(x, y - 1))
                neighbors.Add(map.tiles[x, y - 1]);
            if (map.IsValidTile(x, y + 1))
                neighbors.Add(map.tiles[x, y + 1]);
            return neighbors;
        }

        public List<TileModel> GetVicinity(MapModel map)
        {
            List<TileModel> neighbors = GetClosestVicinity(map);

            if (map.IsValidTile(x + 1, y + 1))
                neighbors.Add(map.tiles[x + 1, y + 1]);
            if (map.IsValidTile(x + 1, y - 1))
                neighbors.Add(map.tiles[x + 1, y - 1]);
            if (map.IsValidTile(x - 1, y + 1))
                neighbors.Add(map.tiles[x - 1, y + 1]);
            if (map.IsValidTile(x - 1, y - 1))
                neighbors.Add(map.tiles[x - 1, y - 1]);
            return neighbors;
        }
    }
}