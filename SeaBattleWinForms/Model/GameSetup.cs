﻿namespace SeaBattle.Model
{
    public class GameSetup
    {
        public bool Multiplayer { get; set; }
        public string PlayerName { get; set; }
        public string SecondPlayerName { get; set; }
    }
}
