﻿using SeaBattle.Utililty;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace SeaBattle.Model
{
    public class Ship
    {
        public readonly Point location;
        public readonly int size;
        public readonly Rotation rotation;
        private int hp;

        public Ship(Point location, int size, Rotation rotation)
        {
            this.location = location;
            this.size = size;
            this.rotation = rotation;

            hp = size;
        }

        public List<TileModel> GetTiles(MapModel map)
        {
            List<TileModel> list = new List<TileModel>();

            for (int i = 0; i < size; i++)
            {
                if (rotation == Rotation.Horizontal)
                    list.Add(map.tiles[location.X + i, location.Y]);
                else
                    list.Add(map.tiles[location.X, location.Y + i]);
            }

            return list;
        }

        internal bool IsDestroyed()
        {
            return hp == 0;
        }

        public List<TileModel> GetVicinity(MapModel map)
        {
            var shipTiles = GetTiles(map);

            HashSet<TileModel> list = new HashSet<TileModel>();
            foreach (var tile in shipTiles)
                foreach (var neighbor in tile.GetVicinity(map))
                        if (!shipTiles.Contains(neighbor)) //dont put this ship's tiles to vicinity
                            list.Add(neighbor);

            return list.ToList();
        }

        public void ReceiveHit()
        {
            hp--;
        }
    }
}
