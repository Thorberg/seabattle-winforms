﻿using SeaBattle.Controller;

namespace SeaBattle.Model
{
    public class Player
    {
        public string Nickname { get; set; }
        public MapModel Board { get; set; }

        public Player Opponent { get; set; }
        public ArtificialIntelligence AI { get; set; }
        public bool ControlledByAI => AI != null;
    }
}
