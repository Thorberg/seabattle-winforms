﻿using SeaBattle.Controller;
using SeaBattle.Model;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace SeaBattle.View
{
    public partial class Tile : UserControl
    {
        private TileModel model;

        private static Image SeaImage = Properties.Resources.Sea;
        private static Image FogImage = Properties.Resources.White;
        private static Image ShipImage = Properties.Resources.Black;
        private static Image DestroyedShipImage = Properties.Resources.Destroyed;

        public Tile()
        {
            InitializeComponent();
        }

        internal void SetModel(TileModel tileModel)
        {
            model = tileModel;
        }

        internal void MyUpdate()
        {
            if (model.fogOfWar)
                SetPicture(FogImage);
            else 
            if (!model.ContainShip)
                SetPicture(SeaImage);
            else
            if (!model.ship.IsDestroyed())
                SetPicture(ShipImage);
            else
            if (model.ship.IsDestroyed())
                SetPicture(DestroyedShipImage);

            if (model.IsRevealed)
                Enabled = false;
            else
                Enabled = true;
        }

        private void SetPicture(Image image)
        {
            if (pictureBox1.Image == null || !pictureBox1.Image.Equals(image))
                pictureBox1.Image = image;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Program.Controller.TileClicked(model);
        }
    }
}
