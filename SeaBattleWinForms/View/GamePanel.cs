﻿using SeaBattle.Controller;
using System;
using System.Windows.Forms;

namespace SeaBattle.View
{
    public partial class GamePanel : UserControl
    {
        public GamePanel()
        {
            InitializeComponent();
        }

        public void MyUpdate()
        {
            map1.MyUpdate();
            map2.MyUpdate();
            label1.Text = $"Current Player: {Program.Controller.turnManager.CurrentPlayer.Nickname}";

            label4.Text = $"Ships: {map1.model.TotalShips - map1.model.DestroyedShips} / {map1.model.TotalShips}";
            label5.Text = $"Ships: {map2.model.TotalShips - map2.model.DestroyedShips} / {map2.model.TotalShips}";
        }

        public void SetPlayerNames(string player1, string player2)
        {
            label2.Text = player1 + "'s board";
            label3.Text = player2 + "'s board";
        }

        public void ShowGameEnd(string winner, int score1, int score2)
        {
            endGamePanel1.Visible = true;
            endGamePanel1.SetData(winner, score1, score2);
        }
    }
}
