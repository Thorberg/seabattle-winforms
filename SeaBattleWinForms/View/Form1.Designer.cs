﻿namespace SeaBattle.View
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.mainMenuPanel1 = new SeaBattle.View.MainMenuPanel();
            this.gamePanel1 = new SeaBattle.View.GamePanel();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 15;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // mainMenuPanel1
            // 
            this.mainMenuPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.mainMenuPanel1.Location = new System.Drawing.Point(459, 63);
            this.mainMenuPanel1.Name = "mainMenuPanel1";
            this.mainMenuPanel1.Size = new System.Drawing.Size(389, 364);
            this.mainMenuPanel1.TabIndex = 1;
            // 
            // gamePanel1
            // 
            this.gamePanel1.AutoScroll = true;
            this.gamePanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gamePanel1.Location = new System.Drawing.Point(0, 0);
            this.gamePanel1.Name = "gamePanel1";
            this.gamePanel1.Size = new System.Drawing.Size(1382, 753);
            this.gamePanel1.TabIndex = 0;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1382, 753);
            this.Controls.Add(this.mainMenuPanel1);
            this.Controls.Add(this.gamePanel1);
            this.Name = "Form1";
            this.Text = "Sea Battle";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer timer1;
        public GamePanel gamePanel1;
        public MainMenuPanel mainMenuPanel1;
    }
}

