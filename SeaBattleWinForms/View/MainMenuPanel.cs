﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SeaBattle.Controller;

namespace SeaBattle.View
{
    public partial class MainMenuPanel : UserControl
    {
        public MainMenuPanel()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            label3.Visible = true;

            Program.Controller.StartGame(new Model.GameSetup()
            {
                Multiplayer = radioButton2.Checked,
                PlayerName = textBox1.Text,
                SecondPlayerName = textBox2.Text
            });
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked) //multiplayer
            {
                label2.Enabled = true;
                textBox2.Enabled = true;
            }
            else
            {
                label2.Enabled = false;
                textBox2.Enabled = false;
            }
        }
    }
}
