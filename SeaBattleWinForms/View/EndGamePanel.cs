﻿using System.Windows.Forms;

namespace SeaBattle.View
{
    public partial class EndGamePanel : UserControl
    {
        public EndGamePanel()
        {
            InitializeComponent();
        }

        public void SetData(string winner, int score1, int score2)
        {
            label2.Text = $"Winner: {winner}";
            label3.Text = $"Score: {score1} vs {score2}";
        }
    }
}
