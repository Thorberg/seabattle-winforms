﻿using SeaBattle.Controller;
using System;
using System.Windows.Forms;

namespace SeaBattle.View
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            gamePanel1.Visible = false;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Program.Controller.MyUpdate();
        }

        internal void MyUpdate()
        {
            if (gamePanel1.Visible)
                gamePanel1.MyUpdate();
        }
    }
}
