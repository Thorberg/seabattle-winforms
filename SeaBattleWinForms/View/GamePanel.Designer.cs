﻿namespace SeaBattle.View
{
    partial class GamePanel
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.endGamePanel1 = new SeaBattle.View.EndGamePanel();
            this.map2 = new SeaBattle.View.Map();
            this.map1 = new SeaBattle.View.Map();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Open Sans", 28.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(278, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(458, 65);
            this.label1.TabIndex = 13;
            this.label1.Text = "CurrentPlayer: None";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Open Sans", 18F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(207, 41);
            this.label2.TabIndex = 14;
            this.label2.Text = "Player1 Board";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Open Sans", 18F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(649, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(207, 41);
            this.label3.TabIndex = 15;
            this.label3.Text = "Player1 Board";
            // 
            // endGamePanel1
            // 
            this.endGamePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.endGamePanel1.Location = new System.Drawing.Point(437, 214);
            this.endGamePanel1.Name = "endGamePanel1";
            this.endGamePanel1.Size = new System.Drawing.Size(600, 250);
            this.endGamePanel1.TabIndex = 16;
            this.endGamePanel1.Visible = false;
            // 
            // map2
            // 
            this.map2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.map2.Location = new System.Drawing.Point(656, 143);
            this.map2.Name = "map2";
            this.map2.Size = new System.Drawing.Size(585, 585);
            this.map2.TabIndex = 10;
            // 
            // map1
            // 
            this.map1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.map1.Location = new System.Drawing.Point(7, 143);
            this.map1.Name = "map1";
            this.map1.Size = new System.Drawing.Size(585, 585);
            this.map1.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Open Sans", 18F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(383, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(143, 41);
            this.label4.TabIndex = 17;
            this.label4.Text = "Ships: x/x";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Open Sans", 18F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(1024, 99);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(143, 41);
            this.label5.TabIndex = 18;
            this.label5.Text = "Ships: x/x";
            // 
            // GamePanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.endGamePanel1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.map2);
            this.Controls.Add(this.map1);
            this.Name = "GamePanel";
            this.Size = new System.Drawing.Size(1280, 800);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label label1;
        public Map map2;
        public Map map1;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label3;
        private EndGamePanel endGamePanel1;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label label5;
    }
}
