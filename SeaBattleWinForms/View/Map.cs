﻿using SeaBattle.Controller;
using SeaBattle.Model;
using System.Drawing;
using System.Windows.Forms;

namespace SeaBattle.View
{
    public partial class Map : UserControl
    {
        private int baseOffset = 5;
        public MapModel model;
        private Tile[,] tiles;

        public Map()
        {
            InitializeComponent();
        }

        public void SetModel(MapModel model)
        {
            this.model = model;
            tiles = new Tile[model.size, model.size];

            for (int i = 0; i < model.size; i++)
                for (int j = 0; j < model.size; j++) {
                    var tile = new Tile();
                    tile.SetModel(model.tiles[i, j]);
                    tiles[i,j] = tile;

                    int x = baseOffset + (i * (tile.Size.Width + baseOffset)); //+ (tile.Size.Width / 2);
                    int y = baseOffset + (j * (tile.Size.Height + baseOffset));// + (tile.Size.Height / 2);
                    tile.Location = new Point(x, y);

                    Controls.Add(tile);
                }
        }

        public void MyUpdate()
        {
            foreach (var tile in tiles)
                tile.MyUpdate();

            if (Program.Controller.turnManager.CurrentPlayer.Board.Equals(model))
                Enabled = false;
            else Enabled = true;
        }
    }
}
