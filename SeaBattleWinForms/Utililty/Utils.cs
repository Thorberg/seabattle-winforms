﻿using SeaBattle.Controller;
using System.Collections.Generic;

namespace SeaBattle.Utililty
{
    static class Utils
    {
        public static void Shuffle<T>(List<T> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                int k = Program.Random.Next(list.Count);
                T value = list[k];
                list[k] = list[i];
                list[i] = value;
            }
        }
    }
}
