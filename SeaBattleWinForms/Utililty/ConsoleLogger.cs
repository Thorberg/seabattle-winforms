﻿using System;

namespace SeaBattle.Utililty
{
    public class ConsoleLogger
    {
        public bool DebugMode { get; set; }

        public void LogError(string message)
        {
            Console.WriteLine($"[{DateTime.Now}]: ERROR = '{message}'");
        }

        public void LogInfo(string message)
        {
            if (DebugMode)
                Console.WriteLine($"[{DateTime.Now}]: DEBUG = '{message}'");
        }
    }
}
