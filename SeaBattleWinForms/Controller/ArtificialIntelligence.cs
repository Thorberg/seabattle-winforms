﻿using SeaBattle.Model;
using SeaBattle.Utililty;
using System;
using System.Collections.Generic;

namespace SeaBattle.Controller
{
    public class ArtificialIntelligence
    {
        private readonly Stack<TileModel> plan = new Stack<TileModel>();
        private Player player;

        public TileModel lastMove;
            
        public ArtificialIntelligence(Player player)
        {
            this.player = player;

            var allTiles = player.Opponent.Board.GetTiles();
            Utils.Shuffle(allTiles);
            foreach (var tile in allTiles)
                plan.Push(tile);
        }

        public TileModel SelectTile()
        {
            if (plan.Count == 0)
                throw new Exception("Stack is empty.. no tiles to select");

            if (lastMove != null && lastMove.IsRevealed && lastMove.ContainShip)
                OnHit(lastMove);

            TileModel nextTile = plan.Pop();
            while (nextTile.IsRevealed)
            {
                nextTile = plan.Pop();
            }
            Program.Logger.LogInfo("AI: " + nextTile.x + "," + nextTile.y);

            lastMove = nextTile;
            return nextTile;
        }

        private void OnHit(TileModel tile)
        {
            foreach (var neighbor in tile.GetClosestVicinity(player.Opponent.Board))
                plan.Push(neighbor);
        }
    }
}