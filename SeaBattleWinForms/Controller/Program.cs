﻿using SeaBattle.Utililty;
using SeaBattle.View;
using System;
using System.Windows.Forms;

namespace SeaBattle.Controller
{
    public static class Program
    {
        public static ConsoleLogger Logger { get; private set; }
        public static Random Random { get; private set; }
        public static Controller Controller { get; private set; }
        public static Form1 View { get; private set; }

        static Program()
        {
            Random = new Random();
            Logger = new ConsoleLogger { DebugMode = true };
        }

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Controller = new Controller();

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(View = new Form1());
        }
    }
}
