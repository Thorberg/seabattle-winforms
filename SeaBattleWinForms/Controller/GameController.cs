﻿using SeaBattle.Model;

namespace SeaBattle.Controller
{
    public class Controller
    {
        public TurnManager turnManager;
        private Player player1;
        private Player player2;

        internal void StartGame(GameSetup info)
        {
            Program.Logger.LogInfo("Initializing game...");

            player1 = new Player() { Nickname = info.PlayerName, Board = new MapModel(10) };
            player2 = new Player() { Nickname = info.SecondPlayerName, Board = new MapModel(10) };

            player1.Opponent = player2;
            player2.Opponent = player1;

            player1.Board.owner = player1;
            player2.Board.owner = player2;

            player1.Board.GenerateShips();
            player2.Board.GenerateShips();

            turnManager = new TurnManager(player1, player2);

            if (!info.Multiplayer)
            {
                player2.AI = new ArtificialIntelligence(player2);
                player2.Nickname = "Computer AI";
            }

            Program.Logger.LogInfo("Applying Model");

            Program.View.gamePanel1.map1.SetModel(player1.Board);
            Program.View.gamePanel1.map2.SetModel(player2.Board);

            Program.View.gamePanel1.SetPlayerNames(player1.Nickname, player2.Nickname);

            Program.View.gamePanel1.Visible = true;
            Program.View.mainMenuPanel1.Visible = false;

            Program.Logger.LogInfo("Game Board finished");
        }

        public void MyUpdate()
        {
            UpdateView();
            UpdateAI();
        }

        private void UpdateView()
        {
            Program.View.MyUpdate();
        }

        private void UpdateAI()
        {
            if (turnManager != null && turnManager.CurrentPlayer.ControlledByAI)
                turnManager.ExecuteTurn(turnManager.CurrentPlayer.AI.SelectTile());
        }

        internal void TileClicked(TileModel tile)
        {
            turnManager.ExecuteTurn(tile);
        }

        internal void GameOver(Player winner)
        {
            Program.View.gamePanel1.ShowGameEnd(winner.Nickname, winner.Opponent.Board.GetScore(), winner.Board.GetScore());
            Program.Logger.LogInfo(winner.Nickname + " won the game"); 
        }
    }
}
