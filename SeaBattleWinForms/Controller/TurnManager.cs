﻿using SeaBattle.Model;
using System;

namespace SeaBattle.Controller
{
    public class TurnManager
    {
        public Player CurrentPlayer { get; private set; }

        public TurnManager(Player player1, Player player2)
        {
            CurrentPlayer = player1;
        }

        public void ExecuteTurn(TileModel tile)
        {
            if (tile.IsRevealed || !CurrentPlayer.Opponent.Board.GetTiles().Contains(tile))
            {
                //tile is already revealed or is not part of the opponent's board
                Program.Logger.LogError("Invalid tile was selected!");
                return;
            }

            tile.Reveal();
            if (tile.ContainShip)
            {
                tile.ship.ReceiveHit();
                if (tile.ship.IsDestroyed())
                {
                    Program.Logger.LogInfo("Ship Destroyed!!");
                    CurrentPlayer.Opponent.Board.RevealShipVicinity(tile.ship);
                    if (CurrentPlayer.Opponent.Board.TotalShips == CurrentPlayer.Opponent.Board.DestroyedShips)
                        Program.Controller.GameOver(CurrentPlayer);
                }
            }
            else //ship was not hit
            {
                SwitchTurn();
            }
        }

        private void SwitchTurn()
        {
            CurrentPlayer = CurrentPlayer.Opponent;
        }
    }
}
